﻿using UnityEngine;
using System.Collections;

public class CubeVall : MonoBehaviour {

	public Transform prefab;

	// Use this for initialization
	void Start () {
		for (int y = 1; y < 5; y++) {
			for (int x = 1; x < 5; x++) {
				var prefar = Instantiate(prefab);
				prefar.transform.position = new Vector3(x, y, 0);
			}
		}
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
