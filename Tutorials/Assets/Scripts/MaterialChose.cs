﻿using UnityEngine;
using System.Collections;

public class MaterialChose : MonoBehaviour {

	public Material one;

	public Material two;

	public Material three;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown (KeyCode.Alpha1)) {
			Renderer renderer = GetComponent<Renderer> ();
			renderer.material = one;
		}
		if (Input.GetKeyDown (KeyCode.Alpha2)) {
			Renderer renderer = GetComponent<Renderer> ();
			renderer.material = two;
		}
		if (Input.GetKeyDown (KeyCode.Alpha3)) {
			Renderer renderer = GetComponent<Renderer> ();
			renderer.material = three;
		}
	}

}
