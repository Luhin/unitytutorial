﻿using UnityEngine;
using System.Collections;

public class cubeRotation : MonoBehaviour {


	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		Vector3 up = Camera.main.transform.up;
		Vector3 right = Camera.main.transform.right;
		if (Input.GetKey (KeyCode.UpArrow) ) {
			transform.Rotate (right, 1, Space.World);
		}
		if (Input.GetKey (KeyCode.DownArrow) ) {
			transform.Rotate (right, -1, Space.World);
		}
		if (Input.GetKey (KeyCode.LeftArrow) ) {
			transform.Rotate (up, 1, Space.World);
		}
		if (Input.GetKey (KeyCode.RightArrow) ) {
			transform.Rotate (up, -1, Space.World);
		}

	}
}
