﻿using System;
using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.IO;
using System.Linq;
using UnityEngine.EventSystems;

public class ButtonAdder : MonoBehaviour {

	public Button customButton;

    public GameObject canvas;

    private FileInfo[] prefabsInfo;

    private string prefabDirectory = "Assets/Resources/";

    public GameObject selectedPrefab;

    void Start()
    {
        DirectoryInfo directory = new DirectoryInfo(prefabDirectory);
        prefabsInfo = directory.GetFiles("*.prefab");
        for (int i = 0; i < prefabsInfo.Length; i++)
        {
            Button button = Instantiate(customButton);
            button.transform.SetParent(canvas.transform);
            string prefabFullName = prefabsInfo[i].Name;
            int extensionIndex = prefabFullName.LastIndexOf('.');
            string prefabName = prefabFullName.Substring(0, extensionIndex);
            button.transform.GetChild(0).GetComponent<Text>().text = prefabName;
            button.transform.position = new Vector3(100, (i + 1) * 40);
            button.onClick.AddListener(() => ButtonClicked(button));

        }
    }

	void Update () {
	}

    void ButtonClicked(Button currenButton)
    {
        if (selectedPrefab != null)
        {
            Destroy(this.selectedPrefab);
        }
        GameObject target = Resources.Load(currenButton.transform.GetChild(0).GetComponent<Text>().text) as GameObject;
        selectedPrefab = Instantiate(target);
        }
    }
